import java.sql.Connection;

/**
 * Created by me on 2015-10-18.
 * 1
 * 
 */
public interface ConnectionMaker {
    public Connection makeConnection();
}
